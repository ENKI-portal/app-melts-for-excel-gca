.PHONY: build dist redist install install-from-source clean uninstall

build:
	CYTHONIZE=1 ./setup.py build

dist:
	CYTHONIZE=1 ./setup.py sdist

redist: clean dist

install:
	CYTHONIZE=1 pip install . --use-feature=in-tree-build

install-from-source: dist
	pip install dist/melts-cython-framework-1.1.0.tar.gz

clean:
	$(RM) -r build dist src/*.egg-info
	$(RM) -r src/melts/rMELTSframework.c
	$(RM) -r *.tbl
	$(RM) -r melts.out
	$(RM) -r liquid-model-batch.inp
	find . -name __pycache__ -exec rm -r {} +

uninstall:
	pip uninstall melts-cython-framework
