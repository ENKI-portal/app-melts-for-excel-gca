
# MELTS WS in Python
# Replacement for Objective-C (Bombax-based) web services
# Mark S. Ghiorso, October 2021

import datetime
from flask import Flask, request, render_template
from typing import Optional
from xml.etree.ElementTree import Element, SubElement, tostring
import xmltodict
import melts.rMELTSframework as rMELTSframework
import time
import random
import numpy as np
import dill
import logging

from lxml import etree, objectify
from lxml.etree import XMLSyntaxError

# Google Cloud Datastore libraries and methods

from google.cloud import datastore
datastore_client = datastore.Client()

def store_silminStateInstance(sessionID: str, ss: str) -> None:
    session_key = datastore_client.key('sessionID', sessionID)
    session = datastore.Entity(key=session_key, exclude_from_indexes=['silminState'])
    session['silminState'] = ss
    session['createdTime'] = datetime.datetime.now(datetime.timezone.utc)
    datastore_client.put(session)

def fetch_silminStateInstance(sessionID: str) -> Optional[str]:
    session_key = datastore_client.key('sessionID', sessionID)
    query = datastore_client.query(kind='sessionID')
    query.key_filter(session_key)
    result = query.fetch(limit=1)
    ss = next (result, None)
    return ss['silminState'] if ss is not None else None

def fetch_times(limit, since_hours=1):
    query = datastore_client.query(kind='sessionID')
    query.add_filter('createdTime', '<', 
        datetime.datetime.now(datetime.timezone.utc)-datetime.timedelta(hours=since_hours))
    query.order = ['-createdTime']
    times = query.fetch(limit=limit)
    return times

# C-structure SilminState shadow Python class

class SilminStateShadow:
    def __init__(self, nc: int, npc: int, modelType: int) -> None:
        self.nc: int                        = nc
        self.npc: int                       = npc
        self.modelType: int                 = modelType
        self.bulkComp: list[float]          = [0]*nc # double *
        self.dspBulkComp: list[float]       = [0]*nc # double *
        # ThermoData bulkTD

        self.liquidComp: list[list[float]]  = [[0]*nc] # double **
        self.nLiquidCoexist: int            = 1 # int
        self.liquidDelta: list[list[float]] = [[0]*nc] # double **
        self.liquidMass: float              = 0 # double
        # ThermoData liquidTD
        self.multipleLiqs: int              = False # int

        self.solidComp: list[list[float]]   = [[0]]*npc # double **
        self.nSolidCoexist: list[int]       = [0]*npc # int *
        self.solidDelta: list[list[float]]  = [[0]]*npc # double **
        self.solidMass: float               = 0 # double
        self.incSolids: list[int]           = [0]*npc # int *
        self.cylSolids: list[int]           = [0]*npc # int *
        # ThermoData solidTD

        self.dspTstart: float               = 0 # double
        self.dspTstop: float                = 0 # double
        self.dspTinc: float                 = 0 # double
        self.dspHstop: float                = 0 # double
        self.dspHinc: float                 = 0 # double
        self.dspSstop: float                = 0 # double
        self.dspSinc: float                 = 0 # double
        self.T: float                       = 0 # double

        self.dspPstart: float               = 0 # double
        self.dspPstop: float                = 0 # double
        self.dspPinc: float                 = 0 # double
        self.dspVstop: float                = 0 # double
        self.dspVinc: float                 = 0 # double
        self.dspDPDt: float                 = 0 # double
        self.dspDPDH: float                 = 0 # double
        self.dspDPDS: float                 = 0 # double
        self.dspDVDt: float                 = 0 # double
        self.P: float                       = 0 # double

        self.fo2: float                     = 0 # double
        self.fo2Path: int                   = 0 # int
        self.fo2Delta: float                = 0 # double
        self.oxygen: float                  = 0 # double

        self.fractionateSol: bool           = False # int
        self.fractionateLiq: bool           = False # int
        self.fractionateFlu: bool           = False # int
        self.fracSComp: list[list[float]]   = [[0]]*npc # double **
        self.nFracCoexist: list[int]        = [0]*npc # int *
        self.fracLComp: list[float]         = [0]*nc  # double *
        self.fracMass: float                = 0 # double

        self.isenthalpic: bool              = False # int
        self.refEnthalpy: float             = 0 # double
        self.isentropic: bool               = False # int
        self.refEntropy: float              = 0 # double
        self.tDelta: float                  = 0 # double

        self.isochoric: bool                = False # int
        self.refVolume: float               = 0 # double
        self.pDelta: float                  = 0 # double

        self.assimilateb: bool              = False # int
        self.dspAssimComp: list[list[float]] = [[0]]*npc # double **
        self.nDspAssimComp: list[int]       = [0]*npc # int *
        self.dspAssimUnits: int             = 0 # int
        self.dspAssimMass: float            = 0 # double
        self.dspAssimT: float               = 0 # double
        self.dspAssimInc: int               = 0 # int
        self.dspAssimLiqM: float            = 0 # double
        self.assimComp: list[list[float]]   = [[0]]*nc # double **
        self.nAssimComp: list[int]          = [0]*nc # int *
        self.assimMass: float               = 0 # double
        self.assimT: float                  = 0 # double
        self.assimInc: int                  = 0 # int
        # ThermoData assimTD

        self.plotState: bool                = False # int
        self.ySol: list[float]              = [0]*npc # double *
        self.yLiq: list[float]              = [0]*nc # double *
    def __repr__(self):
        return "SilminStateShadow(nc, npc, modelType)"
    def __str__(self):
        return ('SilminState Shadow Class: members:' + '\n'
                + '->temperature' + '\n' + str(self.T) + '\n'
                + '->pressure' + '\n' + str(self.P) + '\n'
                + '->bulkCpomp' + '\n' + str(self.bulkComp) + '\n'
                + '->liquidComp' + '\n' + str(self.liquidComp) + '\n'
                + '->liquidMass' + '\n' + str(self.liquidMass) + '\n'
                + '->multipleLiqs' + '\n' + str(self.multipleLiqs) + '\n'
                + '->nSolidCoexist' + '\n' + str(self.nSolidCoexist) + '\n'
                + '->solidComp' + '\n' + str(self.solidComp) + '\n'
                + '->solidMass' + '\n' + str(self.solidMass) + '\n'
                + '->incSolids' + '\n' + str(self.incSolids) + '\n'
                + '->fO2' + '\n' + str(self.fo2) + '\n'
                + '->fO2Path' + '\n' + str(self.fo2Path) + '\n'
                + '->fO2Delta' + '\n' + str(self.fo2Delta) + '\n'
                + '->oxygen' + '\n' + str(self.oxygen) + '\n'
                + '->fractionateSol' + '\n' + str(self.fractionateSol) + '\n'
                + '->fractionateLiq' + '\n' + str(self.fractionateLiq) + '\n'
                + '->fractionateFlu' + '\n' + str(self.fractionateFlu) + '\n'
                + '->nFracCoexist' + '\n' + str(self.nFracCoexist) + '\n'
                + '->fracSComp' + '\n' + str(self.fracSComp) + '\n'
                + '->fracLComp' + '\n' + str(self.fracLComp) + '\n'
                + '->fracMass' + '\n' + str(self.fracMass) + '\n'
               )

def xml_validator(some_xml_string, xsd_file='./static/MELTSinput.xsd'):
    try:
        schema = etree.XMLSchema(file=xsd_file)
        parser = objectify.makeparser(schema=schema)
        objectify.fromstring(some_xml_string, parser)
        return True
    except XMLSyntaxError as e:
        #handle exception here
        print (e)
        print ("Oh NO!, my xml file does not validate")
        pass

def load_silminStateShadow(ss: SilminStateShadow):
    ss.bulkComp       = rMELTSframework.cy_bulkCompAsList()
    ss.nLiquidCoexist = rMELTSframework.cy_nLiquidCoexist()
    ss.liquidComp     = rMELTSframework.cy_liquidCompAsListOfLists()
    ss.liquidMass     = rMELTSframework.cy_liquidMass()
    ss.multipleLiqs   = rMELTSframework.cy_multipleLiqs()

    ss.nSolidCoexist  = rMELTSframework.cy_nSolidCoexistAsList()
    ss.solidComp      = rMELTSframework.cy_solidCompAsListOfLists()
    ss.solidMass      = rMELTSframework.cy_solidMass()
    ss.incSolids      = rMELTSframework.cy_incSolidsAsList()

    ss.dspTstart      = rMELTSframework.cy_initialTemperature()
    ss.dspTstop       = rMELTSframework.cy_finalTemperature()
    ss.dspTinc        = rMELTSframework.cy_incrementTemperature()
    ss.dspHstop       = rMELTSframework.cy_finalEnthalpy()
    ss.dspHinc        = rMELTSframework.cy_incrementEnthalpy()
    ss.dspSstop       = rMELTSframework.cy_finalEntropy()
    ss.dspSinc        = rMELTSframework.cy_incrementEntropy()
    ss.T              = rMELTSframework.cy_initialTemperature()

    ss.dspPstart      = rMELTSframework.cy_initialPressure()
    ss.dspPstop       = rMELTSframework.cy_finalPressure()
    ss.dspPinc        = rMELTSframework.cy_incrementPressure()
    ss.dspVstop       = rMELTSframework.cy_finalVolume()
    ss.dspVinc        = rMELTSframework.cy_incrementVolume()
    ss.P              = rMELTSframework.cy_initialPressure()

    ss.fo2            = rMELTSframework.cy_fo2()
    ss.fo2Path        = rMELTSframework.cy_fo2Path()
    ss.fo2Delta       = rMELTSframework.cy_fo2Delta()
    ss.oxygen         = rMELTSframework.cy_oxygenContent()

    ss.fractionateSol = rMELTSframework.cy_fractionateSol()
    ss.fractionateLiq = rMELTSframework.cy_fractionateLiq()
    ss.fractionateFlu = rMELTSframework.cy_fractionateFlu()
    ss.nFracCoexist   = rMELTSframework.cy_nFracCoexistAsList()
    ss.fracSComp      = rMELTSframework.cy_fracSCompAsListOfLists()
    ss.fracLComp      = rMELTSframework.cy_fracLCompAsList()
    ss.fracMass       = rMELTSframework.cy_fracMass()

    ss.isenthalpic    = False if rMELTSframework.cy_isIsenthalpic() == 0 else True
    ss.refEnthalpy    = rMELTSframework.cy_initialEnthalpy()
    ss.isentropic     = False if rMELTSframework.cy_isIsotropic() == 0 else True
    ss.refEntropy     = rMELTSframework.cy_initialEntropy()

    ss.isochoric      = False if rMELTSframework.cy_isIsochoric() == 0 else True
    ss.refVolume      = rMELTSframework.cy_initialVolume()

    # not used currently
    #ss.assimilateb    = False # int
    #ss.dspAssimComp   = [0 for i in range (0,ss.npc)] # double **
    #ss.nDspAssimComp  = [0 for i in range (0,ss.npc)] # int *
    #ss.dspAssimUnits  = 0 # int
    #ss.dspAssimMass   = 0 # double
    #ss.dspAssimT      = 0 # double
    #ss.dspAssimInc    = 0 # int
    #ss.dspAssimLiqM   = 0 # double
    #ss.assimComp      = [0 for i in range (0,ss.nc)] # double **
    #ss.nAssimComp     = [0 for i in range (0,ss.nc)] # int *
    #ss.assimMass      = 0 # double
    #ss.assimT         = 0 # double
    #ss.assimInc       = 0 # int

def retrieve_silminStateShadow(ss: SilminStateShadow):
    rMELTSframework.cy_setBulkCompAsList(ss.bulkComp)
    rMELTSframework.cy_setnLiquidCoexist(ss.nLiquidCoexist)
    rMELTSframework.cy_setLiquidCompAsListOfLists(ss.liquidComp)
    rMELTSframework.cy_setLiquidMass(ss.liquidMass)
    rMELTSframework.cy_setMultipleLiqs(ss.multipleLiqs)

    rMELTSframework.cy_setnSolidCoexistAsList(ss.nSolidCoexist)
    rMELTSframework.cy_setSolidCompAsListOfLists(ss.solidComp)
    rMELTSframework.cy_setSolidMass(ss.solidMass)
    rMELTSframework.cy_setIncSolidsAsList(ss.incSolids)

    rMELTSframework.cy_setFinalTemperature(ss.dspTstop)
    rMELTSframework.cy_setIncrementTemperature(ss.dspTinc)
    rMELTSframework.cy_setFinalEnthalpy(ss.dspHstop)
    rMELTSframework.cy_setIncrementEnthalpy(ss.dspHinc)
    rMELTSframework.cy_setFinalEntropy(ss.dspSstop)
    rMELTSframework.cy_setIncrementEntropy(ss.dspSinc)
    rMELTSframework.cy_setInitialTemperature(ss.T)

    rMELTSframework.cy_setFinalPressure(ss.dspPstop)
    rMELTSframework.cy_setIncrementPressure(ss.dspPinc)
    rMELTSframework.cy_setFinalVolume(ss.dspVstop)
    rMELTSframework.cy_setIncrementVolume(ss.dspVinc)
    rMELTSframework.cy_setInitialPressure(ss.P)

    rMELTSframework.cy_setfo2(ss.fo2)
    rMELTSframework.cy_setfo2Path(ss.fo2Path)
    rMELTSframework.cy_setfo2Delta(ss.fo2Delta)
    rMELTSframework.cy_setOxygenContent(ss.oxygen)

    rMELTSframework.cy_setFractionateSol(ss.fractionateSol)
    rMELTSframework.cy_setFractionateLiq(ss.fractionateLiq)
    rMELTSframework.cy_setFractionateFlu(ss.fractionateFlu)
    rMELTSframework.cy_setnFracCoexistAsList(ss.nFracCoexist)
    rMELTSframework.cy_setFracSCompAsListOfLists(ss.fracSComp)
    rMELTSframework.cy_setFracLCompAsList(ss.fracLComp)
    rMELTSframework.cy_setFracMass(ss.fracMass)

    rMELTSframework.cy_setIsenthalpic(1 if ss.isenthalpic else 0)
    rMELTSframework.cy_setInitialEnthalpy(ss.refEnthalpy)
    rMELTSframework.cy_setIsotropic(1 if ss.isentropic else 0)
    rMELTSframework.cy_setInitialEntropy(ss.refEntropy)

    rMELTSframework.cy_setIsochoric(1 if ss.isochoric else 0)
    rMELTSframework.cy_setInitialVolume(ss.refVolume)

    # not used currently
    #ss.assimilateb    = False # int
    #ss.dspAssimComp   = [0 for i in range (0,ss.npc)] # double **
    #ss.nDspAssimComp  = [0 for i in range (0,ss.npc)] # int *
    #ss.dspAssimUnits  = 0 # int
    #ss.dspAssimMass   = 0 # double
    #ss.dspAssimT      = 0 # double
    #ss.dspAssimInc    = 0 # int
    #ss.dspAssimLiqM   = 0 # double
    #ss.assimComp      = [0 for i in range (0,ss.nc)] # double **
    #ss.nAssimComp     = [0 for i in range (0,ss.nc)] # int *
    #ss.assimMass      = 0 # double
    #ss.assimT         = 0 # double
    #ss.assimInc       = 0 # int


app = Flask(__name__)

@app.route('/')
def root():
    return render_template('index.html')

@app.errorhandler(500)
def server_error(e):
    logging.exception('An error occurred during a request.')
    return """
    An internal error occurred: <pre>{}</pre>
    See logs for full stacktrace.
    """.format(e), 500

@app.route('/Compute', methods=['POST', 'GET'])
def BaseProcessor():
    return Test(debug=False)

@app.route('/Test', methods=['POST', 'GET'])
def Test(debug=True):
    if debug:
        print ("HEADERS", request.headers)
        print ("REQ_path", request.path)
        print ("ARGS",request.args)
        print ("DATA",request.data)
        print ("FORM",request.form)
        print ("Cookies", request.cookies)
        print (request.get_data())
    xml_string = request.get_data().decode("utf-8")
    if debug:
        print (xml_string)
    xml_validator(xml_string)

    inpRequest = xmltodict.parse(request.get_data())
    
    sessionID = None
    if "sessionID" in request.cookies:
        sessionID = request.cookies['sessionID']
        if debug:
            print ("Found sessionID cookie", sessionID)
    else:
        if debug:
            print ("No sessionID cookie. Checking input XML ...")
        sessionID = inpRequest['MELTSinput']['sessionID']
        if debug:
            if sessionID is None:
                print ("No sessionID in input XML.")
            else:
                print ("Found sessionID in XML input", sessionID)

    if sessionID is not None:
        if "initialize" in inpRequest['MELTSinput']:
            if debug:
                print ("Found 'initialize' in XML input. Negating sessionID.")
            sessionID = None

    # If sessionID exists attempt to retrieve a valid silminState
    if sessionID is not None:
        # Test retrieval
        if debug:
            print ("Retrieving serialized state from datastore ...")
        serializedState = fetch_silminStateInstance(sessionID)
        if serializedState == None:
            sessionID = None
            if debug:
                print ("... sessionID key not found in datastore.")
        else:
            if debug:
                print ("... Done.")
                print (" Reload silminState from dill dump ...")
            ss_reload = dill.loads(serializedState)
            modelType = ss_reload.modelType
            if debug:
                print (f'Reinitializing primary datastructures with saved modelType = {modelType}')
            rMELTSframework.cy_init(modelType)
            if debug:
                print ("... Done.")
                print ("Now create a default SilminState structure ...")
            rMELTSframework.cy_createcreateDefaultSilminState()
            if debug:
                print ("... Done.")
                print (f'nc,def = {ss_reload.nc},{rMELTSframework.cy_num_oxides()}, npc,def = {ss_reload.npc},{rMELTSframework.cy_num_phase_components()}')
                print ("Now pass the reloaded class instance back to the C structure ...")
            retrieve_silminStateShadow(ss_reload)
            if debug:
                print ("... Done.")

    # No previous state identified
    if sessionID is None:
        modelType = rMELTSframework.cy_MELTScalculationModeConstant()       
        if "modelSelection" in inpRequest['MELTSinput']['initialize']:
            version = inpRequest['MELTSinput']['initialize']['modelSelection']
            modelType = None
            if version == 'MELTS_v1.0.x':
                modelType = rMELTSframework.cy_MELTScalculationModeConstant()
            elif version == 'MELTS_v1.1.x':
                modelType = rMELTSframework.cy_MELTSandCO2calculationModeConstant()
            elif version == 'MELTS_v1.2.x':
                modelType = rMELTSframework.cy_MELTSandCO2_H2OcalculationModeConstant()
            elif version == 'MELTS_v5.6.1':
                modelType = rMELTSframework.cy_pMELTScalculationModeConstant()
            if modelType is None:
                return "MELTS WS Phases - Incorrect MELTS model specified", 500, {}
            if debug:
                print ("Found model selection: ", version)
        elif debug:
            print ("Set model selection to MELTS_v1.0.x.")
        sessionID = str(time.time())+str(random.random())
        if debug:
            print ("Generated sessionID: ", sessionID)
        if rMELTSframework.cy_init(modelType) != 1:
            if debug:
                print ("MELTS WS Phases - internal error in init()")
            return "MELTS WS Phases - internal error in init()", 500, {}
    
    # If the XML parse finds an <initialize> tag, then the default SilminState
    # structure is created in the next call. Otherwise, it is initialized
    # above when the previously saved structure is retrieved.
    calculation_type = rMELTSframework.cy_parseAndLoadDataStructuresFromXMLstring(xml_string)
    if calculation_type == 0:
        if debug:
            print ("MELTS WS Phases - input XML parse error")
        return "MELTS WS Phases - input XML parse error", 500, {}
    elif debug:
        print ("Input parse success! Calculation type", calculation_type)

    isenthalpic = True if rMELTSframework.cy_isIsenthalpic() == 1 else False
    isentropic  = True if rMELTSframework.cy_isIsotropic()   == 1 else False
    isochoric   = True if rMELTSframework.cy_isIsochoric()   == 1 else False
    varOneInitial   = 0.0
    varOneFinal     = 0.0
    varOneIncrement = 0.0

    if not isenthalpic and  not isentropic:
        varOneInitial   = rMELTSframework.cy_initialTemperature()
        varOneFinal     = rMELTSframework.cy_finalTemperature()
        varOneIncrement = rMELTSframework.cy_incrementTemperature()
        rMELTSframework.cy_setFinalTemperature(varOneInitial)
        rMELTSframework.cy_setIncrementTemperature(0.0)
        if varOneIncrement == 0.0:
            varOneFinal = varOneInitial

    elif isenthalpic:
        varOneInitial   = rMELTSframework.cy_initialEnthalpy()
        varOneFinal     = rMELTSframework.cy_finalEnthalpy()
        varOneIncrement = rMELTSframework.cy_incrementEnthalpy()
        rMELTSframework.cy_setInitialEnthalpy(varOneFinal)
        rMELTSframework.cy_setIncrementEnthalpy(0.0)
        varOneInitial = varOneFinal;

    elif isentropic:
        varOneInitial   = rMELTSframework.cy_initialEntropy()
        varOneFinal     = rMELTSframework.cy_finalEntropy()
        varOneIncrement = rMELTSframework.cy_incrementEntropy()
        rMELTSframework.cy_setInitialEntropy(varOneFinal)
        rMELTSframework.cy_setIncrementEntropy(0.0)
        varOneInitial = varOneFinal;

    if debug:
        print ("varOneInitial:", varOneInitial)
        print ("varOneFinal", varOneFinal)
        print ("varOneIncrement", varOneIncrement)

    varTwoInitial   = 0.0
    varTwoFinal     = 0.0
    varTwoIncrement = 0.0

    if not isochoric:
        varTwoInitial   = rMELTSframework.cy_initialPressure()
        varTwoFinal     = rMELTSframework.cy_finalPressure()
        varTwoIncrement = rMELTSframework.cy_incrementPressure()
        rMELTSframework.cy_setFinalPressure(varTwoInitial)
        rMELTSframework.cy_setIncrementPressure(0.0)
        if varTwoIncrement == 0.0:
            varTwoFinal = varTwoInitial

    else:
        varTwoInitial   = rMELTSframework.cy_initialVolume()
        varTwoFinal     = rMELTSframework.cy_finalVolume()
        varTwoIncrement = rMELTSframework.cy_incrementVolume()
        
        rMELTSframework.cy_setInitialVolume(varTwoFinal)
        rMELTSframework.cy_setIncrementVolume(0.0)
        varTwoInitial = varTwoFinal
    if debug:
        print ("varTwoInitial:", varTwoInitial)
        print ("varTwoFinal", varTwoFinal)
        print ("varTwoIncrement", varTwoIncrement)

    continueLoop = False if (varOneIncrement == 0.0) and (varTwoIncrement == 0.0) else True
    meltsSteps = []

    while True:
        continueLoop = False if varOneInitial == varOneFinal else True
        try:
            if debug:
                print ("Perform MELTS calculation ...")
                print ("... start loop: varOneInitial:", varOneInitial)
                print ("... start loop: varTwoInitial:", varTwoInitial)
            success = True if rMELTSframework.cy_performMELTScalculation(calculation_type) == 1 else False
            if not success and debug:
                print ("Unsuccesful MELTS calculation.")
                continueLoop = False
            if debug:
                print ("Generate XML output ...")
            result = rMELTSframework.cy_writeDataStructuresToXMLstring(sessionID)
            meltsSteps.append(result)
            if debug:
                print ("Continue loop ...")

            if varOneFinal < varOneInitial:
                varOneInitial -= varOneIncrement
                if varOneFinal > varOneInitial: 
                    varOneInitial = varOneFinal
            else:
                varOneInitial += varOneIncrement
                if varOneFinal < varOneInitial:
                    varOneInitial = varOneFinal
            
            if varTwoFinal < varTwoInitial:
                varTwoInitial -= varTwoIncrement
                if varTwoFinal > varTwoInitial:
                    varTwoInitial = varTwoFinal
            else:
                varTwoInitial += varTwoIncrement
                if varTwoFinal < varTwoInitial: 
                    varTwoInitial = varTwoFinal
            
            if not isenthalpic and  not isentropic:
                rMELTSframework.cy_setInitialTemperature(varOneInitial)
                rMELTSframework.cy_setFinalTemperature(varOneInitial)
            elif isenthalpic:
                rMELTSframework.cy_setInitialEnthalpy(varOneInitial)
                rMELTSframework.cy_setFinalEnthalpy(varOneInitial)
            elif isentropic:
                rMELTSframework.cy_setInitialEntropy(varOneInitial)
                rMELTSframework.cy_setFinalEntropy(varOneInitial)
            
            if isochoric:
                rMELTSframework.cy_setInitialPressure(varTwoInitial)
                rMELTSframework.cy_setFinalPressure(varTwoInitial)
            else:
                rMELTSframework.cy_setInitialVolume(varTwoInitial)
                rMELTSframework.cy_setFinalVolume(varTwoInitial)
            if debug:
                print ("... end loop: varOneInitial:", varOneInitial)
                print ("... end loop: varTwoInitial:", varTwoInitial)

        except Exception as e:
            continueLoop = False
            if debug:
                print ("An exception occured during a MELTS calculation.")
                print (e)
                print ("... Aborting loop.")
                print ("... Nullifying sessionID.")
            sessionID = None
        if not continueLoop:
            break

    # Capture the final silminState and save it to the datastore
    nc = rMELTSframework.cy_num_oxides()
    npc = rMELTSframework.cy_num_phase_components()
    ss = SilminStateShadow(nc, npc, modelType)
    load_silminStateShadow(ss)
    serializedState = dill.dumps(ss)
    if debug:
        print ("Loading serialized state into datastore ...")
    store_silminStateInstance(sessionID, serializedState)
    if debug:
        print ("... Done.")
        print ("Destroy the silminState structure ...")
    rMELTSframework.cy_destroyDefaultSilminState()
    if debug:
        print ("... Done.")

    # Prune the datastore to remove aged entries
    if debug:
        print ("Storage times ...")
    results = fetch_times(1000)
    batch = datastore_client.batch()
    batch.begin()
    for result in results:
        duration = datetime.datetime.now(datetime.timezone.utc) - result['createdTime']
        if debug:
            print (result['createdTime'], 'age (hrs)', duration.total_seconds()/3600)
            print ('Expired key', result.key)
        batch.delete(result.key)
    batch.commit()
    if debug:
        print ('... Done.')
    
    # Return results
    if len(meltsSteps) > 1:
        result = ''
        for x in meltsSteps:
            result += x
        result += '</MELTSsequence>'
        result = result.replace('<MELTSoutput>', '<MELTSsequence><MELTSoutput>', 1)
        response = app.response_class(result, mimetype='application/xml')
    else:
        response = app.response_class(meltsSteps[0], mimetype='application/xml')
    #response.set_cookie('sessionID', value=sessionID)
    return response

@app.route('/Oxides', methods=['POST', 'GET'])
def Oxides():
    root = Element('MELTSWSoxides')
    # Get list of oxides
    oxide_l = rMELTSframework.cy_oxideListAsStrings()
    for name in oxide_l:
        oxide = SubElement(root, 'Oxide')
        oxide.text = '' + name
    # ... end
    return app.response_class(tostring(root), mimetype='application/xml')

@app.route('/Phases', methods=['POST', 'GET'])
def Phases():
    version = request.get_data().decode("utf-8") 
    version = 'MELTS_v1.0.x' if version == '' else version
    if version == 'MELTS_v1.0.x':
        choice = rMELTSframework.cy_MELTScalculationModeConstant()
    elif version == 'MELTS_v1.1.x':
        choice = rMELTSframework.cy_MELTSandCO2calculationModeConstant()
    elif version == 'MELTS_v1.2.x':
        choice = rMELTSframework.cy_MELTSandCO2_H2OcalculationModeConstant()
    elif version == 'MELTS_v5.6.1':
        choice = rMELTSframework.cy_pMELTScalculationModeConstant()
    else:
        return "MELTS WS Phases - argument ERROR", 500, {}
    root = Element('MELTSWSphases')
    # Get list of phases
    phase_l = rMELTSframework.cy_phaseListAsStrings(choice)
    for name in phase_l:
        phase = SubElement(root, 'Phase')
        phase.text = '' + name
    # ... end
    return app.response_class(tostring(root), mimetype='application/xml')

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8080, debug=True)
