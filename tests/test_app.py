def test_index(app, client):
    del app
    res = client.get('/')
    assert res.status_code == 200
    #expected = {'hello': 'world'}
    #assert expected == json.loads(res.get_data(as_text=True))

def test_oxides(app, client):
    del app
    res = client.post('/Oxides')
    assert res.status_code == 200
    expected = '<MELTSWSoxides><Oxide>SiO2</Oxide><Oxide>TiO2</Oxide><Oxide>Al2O3</Oxide><Oxide>Fe2O3</Oxide><Oxide>Cr2O3</Oxide><Oxide>FeO</Oxide><Oxide>MnO</Oxide><Oxide>MgO</Oxide><Oxide>NiO</Oxide><Oxide>CoO</Oxide><Oxide>CaO</Oxide><Oxide>Na2O</Oxide><Oxide>K2O</Oxide><Oxide>P2O5</Oxide><Oxide>H2O</Oxide><Oxide>CO2</Oxide><Oxide>SO3</Oxide><Oxide>Cl2O-1</Oxide><Oxide>F2O-1</Oxide></MELTSWSoxides>'
    assert expected == res.get_data(as_text=True)

def test_phases_10x(app, client):
    del app
    res = client.post('/Phases', data='MELTS_v1.0.x', content_type='text/plain')
    assert res.status_code == 200
    expected = '<MELTSWSphases><Phase>olivine</Phase><Phase>fayalite</Phase><Phase>sphene</Phase><Phase>garnet</Phase><Phase>melilite</Phase><Phase>orthopyroxene</Phase><Phase>clinopyroxene</Phase><Phase>aegirine</Phase><Phase>aenigmatite</Phase><Phase>cummingtonite</Phase><Phase>amphibole</Phase><Phase>hornblende</Phase><Phase>biotite</Phase><Phase>muscovite</Phase><Phase>feldspar</Phase><Phase>quartz</Phase><Phase>tridymite</Phase><Phase>cristobalite</Phase><Phase>nepheline</Phase><Phase>kalsilite</Phase><Phase>leucite</Phase><Phase>corundum</Phase><Phase>sillimanite</Phase><Phase>rutile</Phase><Phase>perovskite</Phase><Phase>spinel</Phase><Phase>rhm-oxide</Phase><Phase>ortho-oxide</Phase><Phase>whitlockite</Phase><Phase>apatite</Phase><Phase>water</Phase><Phase>alloy-solid</Phase><Phase>alloy-liquid</Phase><Phase>olivine</Phase><Phase>fayalite</Phase><Phase>sphene</Phase><Phase>garnet</Phase><Phase>melilite</Phase></MELTSWSphases>'
    assert expected == res.get_data(as_text=True)

def test_phases_11x(app, client):
    del app
    res = client.post('/Phases', data='MELTS_v1.1.x', content_type='text/plain')
    assert res.status_code == 200
    expected = '<MELTSWSphases><Phase>olivine</Phase><Phase>fayalite</Phase><Phase>sphene</Phase><Phase>garnet</Phase><Phase>melilite</Phase><Phase>orthopyroxene</Phase><Phase>clinopyroxene</Phase><Phase>aegirine</Phase><Phase>aenigmatite</Phase><Phase>cummingtonite</Phase><Phase>amphibole</Phase><Phase>hornblende</Phase><Phase>biotite</Phase><Phase>muscovite</Phase><Phase>feldspar</Phase><Phase>quartz</Phase><Phase>tridymite</Phase><Phase>cristobalite</Phase><Phase>nepheline</Phase><Phase>kalsilite</Phase><Phase>leucite</Phase><Phase>corundum</Phase><Phase>sillimanite</Phase><Phase>rutile</Phase><Phase>perovskite</Phase><Phase>spinel</Phase><Phase>rhm-oxide</Phase><Phase>ortho-oxide</Phase><Phase>whitlockite</Phase><Phase>apatite</Phase><Phase>fluid</Phase><Phase>alloy-solid</Phase><Phase>alloy-liquid</Phase><Phase>calcite</Phase><Phase>aragonite</Phase><Phase>magnesite</Phase><Phase>siderite</Phase><Phase>dolomite</Phase><Phase>spurrite</Phase><Phase>tilleyite</Phase><Phase>diamond</Phase><Phase>graphite</Phase><Phase>olivine</Phase></MELTSWSphases>'
    assert expected == res.get_data(as_text=True)

def test_phases_12x(app, client):
    del app
    res = client.post('/Phases', data='MELTS_v1.2.x', content_type='text/plain')
    assert res.status_code == 200
    expected = '<MELTSWSphases><Phase>olivine</Phase><Phase>fayalite</Phase><Phase>sphene</Phase><Phase>garnet</Phase><Phase>melilite</Phase><Phase>orthopyroxene</Phase><Phase>clinopyroxene</Phase><Phase>aegirine</Phase><Phase>aenigmatite</Phase><Phase>cummingtonite</Phase><Phase>amphibole</Phase><Phase>hornblende</Phase><Phase>biotite</Phase><Phase>muscovite</Phase><Phase>feldspar</Phase><Phase>quartz</Phase><Phase>tridymite</Phase><Phase>cristobalite</Phase><Phase>nepheline</Phase><Phase>kalsilite</Phase><Phase>leucite</Phase><Phase>corundum</Phase><Phase>sillimanite</Phase><Phase>rutile</Phase><Phase>perovskite</Phase><Phase>spinel</Phase><Phase>rhm-oxide</Phase><Phase>ortho-oxide</Phase><Phase>whitlockite</Phase><Phase>apatite</Phase><Phase>fluid</Phase><Phase>alloy-solid</Phase><Phase>alloy-liquid</Phase><Phase>calcite</Phase><Phase>aragonite</Phase><Phase>magnesite</Phase><Phase>siderite</Phase><Phase>dolomite</Phase><Phase>spurrite</Phase><Phase>tilleyite</Phase><Phase>diamond</Phase><Phase>graphite</Phase><Phase>olivine</Phase></MELTSWSphases>'
    assert expected == res.get_data(as_text=True)

def test_phases_561(app, client):
    del app
    res = client.post('/Phases', data='MELTS_v5.6.1', content_type='text/plain')
    assert res.status_code == 200
    expected = '<MELTSWSphases><Phase>olivine</Phase><Phase>sphene</Phase><Phase>garnet</Phase><Phase>melilite</Phase><Phase>orthopyroxene</Phase><Phase>clinopyroxene</Phase><Phase>aegirine</Phase><Phase>aenigmatite</Phase><Phase>cummingtonite</Phase><Phase>amphibole</Phase><Phase>hornblende</Phase><Phase>biotite</Phase><Phase>muscovite</Phase><Phase>feldspar</Phase><Phase>quartz</Phase><Phase>tridymite</Phase><Phase>cristobalite</Phase><Phase>nepheline ss</Phase><Phase>kalsilite ss</Phase><Phase>leucite ss</Phase><Phase>corundum</Phase><Phase>rutile</Phase><Phase>perovskite</Phase><Phase>spinel</Phase><Phase>rhm oxide</Phase><Phase>ortho oxide</Phase><Phase>whitlockite</Phase><Phase>apatite</Phase><Phase>water</Phase><Phase>alloy-solid</Phase><Phase>alloy-liquid</Phase></MELTSWSphases>'
    assert expected == res.get_data(as_text=True)

import re

def test_compute_tp(app, client):
    del app
    init_xml = '<MELTSinput><initialize><modelSelection>MELTS_v1.0.x</modelSelection><SiO2>74.39</SiO2><TiO2>0.18</TiO2><Al2O3>13.55</Al2O3><Fe2O3>0.360480704996108</Fe2O3><FeO>0.975635492235283</FeO><MgO>0.5</MgO><CaO>1.43</CaO><Na2O>3.36</Na2O><K2O>5.09</K2O><H2O>10</H2O></initialize><calculationMode>equilibrate</calculationMode><title>MELTS Web Service called from Excel. [created 2014Apr23] (version 1.0.3)</title><sessionID></sessionID><constraints><setTP><initialT>845</initialT><initialP>2000</initialP><fo2Path>nno</fo2Path><fo2Offset>0</fo2Offset></setTP></constraints><suppressPhase>amphibole</suppressPhase><suppressPhase>biotite</suppressPhase><suppressPhase>leucite</suppressPhase><fractionationMode>fractionateNone</fractionationMode></MELTSinput>'
    res = client.post('/Compute', data=init_xml, content_type='text/xml')
    assert res.status_code == 200
    pickup_pre = '<MELTSinput><calculationMode>equilibrate</calculationMode><title>MELTS Web Service called from Excel. [created 2014Apr23] (version 1.0.3)</title><sessionID>'
    pickup_mid = '</sessionID><constraints><setTP><initialT>'
    pickup_post = '</initialT><initialP>2000</initialP></setTP></constraints><suppressPhase>amphibole</suppressPhase><suppressPhase>biotite</suppressPhase><suppressPhase>leucite</suppressPhase><fractionationMode>fractionateNone</fractionationMode></MELTSinput>'
    session_id = re.search('<sessionID>(.*)</sessionID>', res.get_data(as_text=True)).group(1)
    print (session_id)
    for i in range(844,754,-1):
        res = client.post('/Compute', data=pickup_pre+session_id+pickup_mid+str(i)+pickup_post, content_type='text/xml')
        assert res.status_code == 200
