All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.2.0] - 2021-11-11 (ghiorso)

### Changed
- Structure of repository
- Configuration files
- README file

### Added
- Cython interface to C-code
- Python main driver methods

### Deleted
- Objective-C dependencies

## [0.1.0] - 2021-10-22 (ghiorso)

### Added
- Forked ENKI-PORTAL/xMELTS repository
- Removed files not required for this project (created a main branch and preserved original master branch)
- Added Google Cloud Application Python example files
